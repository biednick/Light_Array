/* FRAMEWORK FOR LIGHT/SOUND THING
 * THIS CODE ONLY CONTAINS 2 HARDCODED UNITS.
 * LEDS ARE ON D3 AND D5
 * RELAYS ARE ON D2 AND D4
 * PHOTOTRNASITORS ARE ON A2 AND A3 */

void setup() {
  pinMode(3, OUTPUT); pinMode(5, OUTPUT); pinMode(2, OUTPUT); pinMode(4, OUTPUT); //Start i/o *****NEED MORE EFFICIENT METHOD FOR ALL 54 I/O*****
  digitalWrite(4, HIGH); digitalWrite(2, HIGH);                                   //Set relays to normal condition

}

    bool check1;                                              //Start variable initialization *****THESE SHOULD BE MADE VECTORS*****
    bool check2;
    unsigned long timer1;
    unsigned long timer2;                                     //End variable initialization
    
void loop() {
   check2 = volts(A2);
   check1 = volts(A3);
  if(check1 == true){                                         //logic statement checks input on first unit
    digitalWrite(3, HIGH);                                    //Turns on LED      *****VALUES NEED TO BE MADE EQUATIONS BASED ON INPUT PIN*****
    digitalWrite(2, LOW);                                     //Changes relay position
    timer1 = millis();                                        //Sets timer value

  }
  if ((millis() - timer1) >= 3000UL){                         //Logic statement checks time since light was removed
    digitalWrite(3, LOW);                                     //Turns off LED
    digitalWrite(2, HIGH);                                    //Returns relay to normal position
    }
    if(check2 == true){                                       //logic statement checks input on second unit
    digitalWrite(5, HIGH);                                    //Turns on LED     *****VALUES NEED TO BE MADE EQUATIONS BASED ON INPUT PIN*****
    digitalWrite(4, LOW);                                     //Changes relay position
    timer2 = millis();                                        //Sets timer value
  }
  if ((millis() - timer2) >= 3000UL){
    digitalWrite(5, LOW);                                     //Turns off LED
    digitalWrite(4, HIGH);                                    //Resets relay to normal position
  }

}

bool volts(int pin){                                          //Returns a bool value to determine state of light sensor
  float voltage = (analogRead(pin)) * 5 / 1024;               //Reads voltage on analog in
  if (voltage < 3){                                           //Return true if voltage is high
    return false;
  }
  else{                                                       //Else return false
    return true;
  }
}
