/* LIGHT/SOUND FRAMEWORK WITH VECTORS
 * FOR 3 LEDS
 * D2, D3, D4 = LEDS
 * D17, D18, D19 = RELAYS
 * A0, A1, A2 = INPUTS
 */

    int i;                                                     //Initialize variables
    int check[3] = {0,0,0};
    unsigned long timer[3];                                    //End variable initialization
    

void setup() {

  pinMode(2, OUTPUT); pinMode(3, OUTPUT); pinMode(4, OUTPUT);                   //Pinmode for LEDs                *****WOULD LIKE TO IMPLEMENT ALL IN LOOPS*****
  pinMode(17, OUTPUT); pinMode(18, OUTPUT); pinMode(19, OUTPUT);                //Pinmode for relays
  digitalWrite(16, HIGH); digitalWrite(17, HIGH); digitalWrite(19, HIGH);       //Set relays to normal condition
}

void loop() {
  for ( i = 0; i <= 2; i++){                                  //For loop resets every time end of array is reached. 
  check[i] = volts(i);                                        //Checks light input; writes into array.
  
  if(check[i] == 1) {                                         //logic statement checks input on unit[i]
    digitalWrite((i+2), HIGH);                                //Turns on LED      *****VALUES NEED TO BE MADE EQUATIONS BASED ON INPUT PIN*****
    digitalWrite((i + 17), LOW);                              //Changes relay position
    timer[i] = millis();                                      //Sets timer value in array
  }
  
  if ((millis() - timer[i]) >= 3000UL) {                      //Logic statement checks time since light was removed
    digitalWrite((i + 2), LOW);                               //Turns off LED
    digitalWrite(i + 17, HIGH);                               //Returns relay to normal position
  }
 }
}

int volts(int pin) {                                          //Returns a bool value to determine state of light sensor
  float voltage = (analogRead(pin)) * 5 / 1024;               //Reads voltage on analog in
  if (voltage > 3) {                                          //Return true if voltage is high
    return 1;
  }
  else {                                                      //Else return false
    return 0;
  }
}
