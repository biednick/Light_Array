/* LIGHT/SOUND FRAMEWORK WITH VECTORS
   FOR 16 units
   D2-D17 = LEDS
   D18-D33 = RELAYS
   A0-A15 = INPUTS
   *****PINS ARE SLIGHTLY DIFFERENT FEOM REV_3.*****
*/
int i;                                                            //Initialize variables
int check[16] = {0};
unsigned long timer[16];                                          //End variable initialization
void setup() {
  for (int k = 2; k <= 17; k ++) {
    pinMode(k, OUTPUT);                                           //Pinmode for LEDs
    pinMode(k + 16, OUTPUT);                                      //Pinmode for relays
    digitalWrite(k , HIGH);                                       //Turn on each LED
    digitalWrite(k + 16, LOW);                                    //Close each relay
    delay(500);                                                   //For .5 sec
    digitalWrite(k, LOW);                                         //Power off LED   
    digitalWrite(k + 16, HIGH);                                   //Open each relay                       
  }
}
  void loop() {
    for ( i = 0; i <= 2; i++) {                                   //For loop resets every time end of array is reached.
      check[i] = volts(i);                                        //Checks light input; writes into array.
      if (check[i] == 1) {                                        //logic statement checks input on unit[i]
        digitalWrite((i + 2), HIGH);                              //Turns on LED     
        digitalWrite((i + 18), LOW);                              //Changes relay position
        timer[i] = millis();                                      //Sets timer value in array
      }
      if ((millis() - timer[i]) >= 3000UL) {                      //Logic statement checks time since light was removed
        digitalWrite((i + 2), LOW);                               //Turns off LED
        digitalWrite(i + 18, HIGH);                               //Returns relay to normal position
      }
    }
  }
  int volts(int pin) {                                           //Returns a bool value to determine state of light sensor
    float voltage = (analogRead(pin)) * 5 / 1024;                //Reads voltage on analog in
    if (voltage > 3) {                                           //Return true if voltage is high
      return 1;
    }
    else {                                                       //Else return false
      return 0;
    }
  }
