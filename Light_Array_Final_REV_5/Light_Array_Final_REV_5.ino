/* LIGHT/SOUND FRAMEWORK WITH VECTORS
   FOR 16 units
   D2-D17 = LEDS
   D18-D33 = SPEAKERS
   A0-A15 = INPUTS
   *****PINS ARE SLIGHTLY DIFFERENT FEOM REV_3.*****
*/

int i;                                                            //Initialize variables
int check[16] = {0};
unsigned long timer[16]; 
int frequency[] = {440, 493, 523, 587, 659, 698, 784, 880, 440, 493, 523, 587, 659, 698, 784, 880};            //End variable initialization


void setup() {
  for (int k = 2; k <= 17; k ++) {
    pinMode(k, OUTPUT);                                           //Pinmode for LEDs
    //pinMode(k + 16, OUTPUT);                                      //Pinmode for speaker
    digitalWrite(k , HIGH);                                       //Turn on each LED
    tone(k + 16, 440);                                            //plays tone from speaker
    delay(500);                                                   //For .5 sec
    digitalWrite(k, LOW);                                         //Power off LED   
    noTone(k + 16);                                               //turns off each speaker                       
  }
}
  void loop() {
    for ( i = 0; i < 16; i++) {                                   //For loop resets every time end of array is reached.
      check[i] = volts(i);                                        //Checks light input; writes into array.
      
      if (check[i] == 1) {                                        //logic statement checks input on unit[i]
        digitalWrite((i + 2), HIGH);                              //Turns on LED     
        tone((i + 18), frequency[i]);                             //Turns on speaker
        timer[i] = millis();                                      //Sets timer value in array
      }

      if ((millis() - timer[i]) >= 3000UL) {                      //Logic statement checks time since light was removed
        digitalWrite((i + 2), LOW);                               //Turns off LED
        noTone(i + 18);                                           //Turns off speaker
        }
    }
  }

  int volts(int pin) {                                           //Returns a bool value to determine state of light sensor
    float voltage = (analogRead(pin)) * 5 / 1024;                //Reads voltage on analog in
    if (voltage > 3) {                                           //Return true if voltage is high
      return 1;
    }
    else {                                                       //Else return false
      return 0;
    }
  }
