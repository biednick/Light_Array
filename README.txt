This code is for an array of lights controlled by an Arduino Mega. General opperation is as follows:
-Photoresistors (1 per unit) send an analog input signal
-LEDs(1 or more per unit) are turned on using digital outputs
-Relays (1 per unit) are closed using digital outputs, switching an audio signal to a speaker (1 per unit).

When a photoresistor is illuminated by a flashlight, the corresponding LED turns on and the corresponding relay closes. 3 seconds after the flashlight is removed, the LED and relay
return to notmal positions. 

The intention is to have an array with a large number of units consisting of a photoresistor, LED, and speaker hang from a framework on the ceiling.

As of REV_2 30MARCH2017, the code is incomplete and will only operate 3 of a maximum 16 units. Final opperation has not been determined.
UPDATE 30MAR2017: Uploaded REV_3. This version of the code declares pin opperation in for loops, making it more efficient for large numbers of I/O.
UPDATE 31MAR2017: Uploaded REV_4. This version declares pin opperation for 16 units and flashes each LED/relay for .5 sec on pin initialization. 
UPDATE 09APR2017: Uploaded REV_5. This version removes the relays for speaker control and used tone() to play a psuedo random tone through each speaker.